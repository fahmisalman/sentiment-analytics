# Sentiment Analytics of Restaurant Reviews

The purpose of this analysis is to make a predictive model of whether a restaurant review is positive or negative. In this experiment, I used the Long Short Term Memory (LSTM) method to create a classification model, and I observed the use of preprocessing to produce the best model.

## Notes

The tool last tested using the following configuration:

- Python 3.6.9
- TensorFlow 1.15.2
- PySastrawi 1.2.0
- scikit-learn 0.22.2.post1
- Pandas 1.0.3

## Instruction

To run this program: 

    $ python main.py --text "Salah satu cafe yg msh bertahan dan msh ramai pengunjung ya cafe ini. Banyaknya menu makanan yg ditawarkan, rasa yg enak, lokasi, tempat yg cukup nyaman menjadi salah satu pilihan terutama pelanggan lama bisa bertahan. Untuk harga relatif agak mahal tp masih sebanding dengan rasa. Tempat parkir cukup luas, pelayanan standar saja. Untuk cafe lama, masih ok untuk dikunjungi." --model model2

Result

    $ positive

The model arguments is optional. There are 3 models: model1 (without stemming and stopword), model2 (with stemming but without stopword), model3 (with stemming and stopword). If not defined, model1 will be used.
    
## Dataset

The dataset is owned by PT Prosa Solusi Cerdas. \
The dataset is used for assignment only. 
It is not allowed to distributed this dataset without the acknowledgement of PT Prosa Solusi Cerdas.

