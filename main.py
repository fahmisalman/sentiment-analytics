from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.models import load_model

from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
import pandas as pd
import argparse
import pickle
import re

def text_lower(texts):
    return texts.lower()

def text_cleaning(texts):
    return ' '.join((re.sub(r'[^a-z]', ' ', texts)).split())

def text_stemming(texts):
    factory = StemmerFactory()
    stemmer = factory.create_stemmer()
    return stemmer.stem(texts)

def stopwords_removal(texts):
    texts_token = texts.split()
    not_stopword = []
    for token in texts_token:
        if token not in stopwords:
            not_stopword.append(token)
    return ' '.join(not_stopword)

def text_norm(texts):
    words = texts.split()
    for i in range(len(words)):
        try:
            words[i] = normwords.loc[words[i]]['hasil']
        except:
            pass
    return ' '.join(words)

def load_normwords(file):
    return pd.read_csv(file, index_col=['singkat'])

def load_stopwords(file):
    return [line.rstrip('\n\r') for line in open(file)]

def preprocessing(texts):
    tx_lower = text_lower(texts)
    tx_clean = text_cleaning(tx_lower)
    tx_norm = text_norm(tx_clean)
    return tx_norm

ap = argparse.ArgumentParser()
ap.add_argument("-t", "--text", required=True,
	help="Input sentence that will be classified")
ap.add_argument("-m", "--model", required=False,
	help="Model that will be use in classification: model1 (without stemming and stopword), model2 (with stemming but without stopword), model3 (with stemming and stopword). If not defined, model1 will be used")
args = vars(ap.parse_args())

global normwords
global stopwords
normwords = load_normwords("Model/key_norm.csv")

if args['model'] == 'model2':
    with open('Model/tokenizer_stem.pickle', 'rb') as handle:
        tokenizer = pickle.load(handle)
    model = load_model('Model/model_stem.h5')
    text = text_stemming(preprocessing(args['text']))
elif args['model'] == 'model3':
    stopwords = load_stopwords("Model/stopword_list_TALA.txt")
    stopwords.extend(['nya'])
    stopwords.remove('tidak')
    stopwords.remove('enggak')
    stopwords.remove('lama')
    with open('Model/tokenizer_stop.pickle', 'rb') as handle:
        tokenizer = pickle.load(handle)
    model = load_model('Model/model_stop.h5')
    text = stopwords_removal(text_stemming(preprocessing(args['text'])))
else:
    with open('Model/tokenizer_norm.pickle', 'rb') as handle:
        tokenizer = pickle.load(handle)
    model = load_model('Model/model_norm.h5')
    text = preprocessing(args['text'])

def predict(text):
    class_label = ['negative', 'positive']
    tx_tokenizer = tokenizer.texts_to_sequences([text])
    tx_tokenizer_pad = pad_sequences(tx_tokenizer, maxlen=100, padding='pre', truncating='pre')
    return class_label[int(model.predict_classes(tx_tokenizer_pad))]

print(predict(text))